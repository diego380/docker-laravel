# Estrutura do docker para desenvolvimento de aplicações com Laravel (Lemp)
## Docker - Nginx, Mysql, Php (Laravel)

**CREDITOS**:

[Laravel Restful API using Docker in three steps](https://medium.com/@newaeonweb/laravel-restful-api-using-docker-in-three-steps-63a5b4c8f211)

### Passos:
1) Rodar os comandos seguintes no seu terminal de preferencia:

    docker-compose up -d
    docker exec -it php-fpm bash -c "cp .env-example .env && php artisan key:generate"

2) Abrir o navegador e colocar o endereço:

    http://localhost:8081

3) Acesso ao phpMyAdmin, abrir o navegador e colocar o seguinte endereço:

    http://localhost:8082

    Dados de acesso:
        
        1) Acesso como root:    
            -Servidor: meuip 3308 // ex.: 192.168.0.1 3308
            -Usuario: root
            -Senha: root
        
        2) Acesso como admin:    
            -Servidor: meuip 3308 // ex.: 192.168.0.1 3308
            -Usuario: admin
            -Senha: admin

4) Só aproveitar! **ENJOY**